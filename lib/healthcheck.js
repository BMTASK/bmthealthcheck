import { WebApp } from 'meteor/webapp';
import { Meteor } from 'meteor/meteor'
import pusage from 'pidusage'

function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}

const HEALTH_CHECK_RESPONSE = {
	success:1
}

WebApp.rawConnectHandlers.use('/health-check', (req, res, cb) => {
	res.writeHead(200, {
		'Content-Type': 'text/plain'    
	});
	pusage.stat(process.pid, function(err, stat) {
		HEALTH_CHECK_RESPONSE.cpu=stat.cpu
		HEALTH_CHECK_RESPONSE.memory=formatBytes(stat.memory)
		HEALTH_CHECK_RESPONSE.sessions=_.keys(Meteor.default_server.sessions).length
		// console.log(HEALTH_CHECK_RESPONSE,pusage,res)
		pusage.unmonitor(process.pid);
		res.end(JSON.stringify(HEALTH_CHECK_RESPONSE));
	})
});