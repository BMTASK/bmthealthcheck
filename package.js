Package.describe({
	summary: "Helth check service",
	name: "bmt:healthcheck",
	version: "0.1.0",
	git: ""
});

Npm.depends({
	'pidusage': '1.1.6'
});
  
Package.on_use(function(api) {
	api.use([
		'webapp',
		'ecmascript',
		'underscore',
		'meteor'
	]);
	api.add_files([
		'lib/healthcheck.js'
	], ['server']);
});